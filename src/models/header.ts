export interface Header {
  description: string;
  key: string | null;
}
