export interface User {
  id: number;
  fullName: string;
  email: string;
  gender: string;
  status: boolean;
}
