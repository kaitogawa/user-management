const colors = {
  primary: '#1890ff',
  secondary: '#6AB3F3',
  steelBlue: '#2d61b9',
  forestGreen: '#249e1f',
  steelBlueRGB: '45, 97, 185',
  sandyBrown: '#f9ae3c',
  white: '#fff',
  whiteSmoke: '#eee',
  danger: '#ff4d4f',
  primaryRGB: '24, 143, 255',
};

export default colors;
