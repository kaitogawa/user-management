import React from 'react';
import './App.css';
import RouterRoot from './routes';

const App: React.FC = () => {
  return (
    <div className="App">
      <RouterRoot />
    </div>
  );
};

export default App;
