import React from 'react';
import { shallow } from 'enzyme';
import { CustomButton } from './styled';

describe('CustomButton', () => {
  it('renders without crashing', () => {
    const Button = shallow(<CustomButton>testing</CustomButton>);
    expect(Button).toMatchSnapshot();
  });
});
