import styled from 'styled-components';
import colors from '../../theme/colors';

const CustomButton = styled.button`
  background-color: rgba(${colors.primaryRGB}, ${props =>
  props.disabled ? 0.5 : 1}); /* Green */
  cursor: ${props => (props.disabled ? 'default' : 'pointer')}
  border: none;
  color: white;
  padding: 8px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 0px 5px;
  border-radius: 5px;
  transition: 0.3s all ease-out;

  &:hover {
    background-color: rgba(${colors.primaryRGB}, 0.5);
  }
`;

export { CustomButton };
