import React from 'react';
import PropTypes from 'prop-types';
import { StyledHeader } from './styled';
import { Header } from '../../models/header';
import { FaAngleUp, FaAngleDown } from 'react-icons/fa';
import colors from '../../theme/colors';

interface Props {
  columns: Header[];
  sortType: string;
  sortColumn: string | null;
  setSortType: (type: string) => void;
  setSortColumn: (column: string) => void;
}

export const TableHeader: React.FC<Props> = props => {
  const { columns, sortColumn, sortType, setSortType, setSortColumn } = props;

  return (
    <thead style={{ color: 'white', top: 0 }}>
      <tr>
        {columns.map((column, index) => {
          return (
            <StyledHeader
              key={index}
              style={{ cursor: column.key ? 'pointer' : 'default' }}
              onClick={() => {
                if (column.key) {
                  if (sortColumn && column.key === sortColumn) {
                    setSortType(sortType === 'asc' ? 'desc' : 'asc');
                  }
                  if (column.key !== sortColumn) {
                    setSortColumn(column.key);
                  }
                }
              }}
            >
              <div
                style={{
                  flexDirection: 'row',
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                {column.description}
                {column.key &&
                  (sortColumn === column.key ? (
                    sortType === 'desc' ? (
                      <FaAngleUp
                        color={colors.white}
                        style={{ marginLeft: 10 }}
                      />
                    ) : (
                      <FaAngleDown
                        color={colors.white}
                        style={{ marginLeft: 10 }}
                      />
                    )
                  ) : (
                    <div>
                      <FaAngleUp
                        color={colors.white}
                        style={{ marginLeft: 10 }}
                      />
                      <FaAngleDown color={colors.white} />
                    </div>
                  ))}
              </div>
            </StyledHeader>
          );
        })}
      </tr>
    </thead>
  );
};

TableHeader.propTypes = {
  columns: PropTypes.array.isRequired,
  sortType: PropTypes.string.isRequired,
  sortColumn: PropTypes.string,
  setSortType: PropTypes.func.isRequired,
  setSortColumn: PropTypes.func.isRequired,
};
