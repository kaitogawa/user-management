import styled from 'styled-components';
import colors from '../../theme/colors';

const StyledTable = styled.table`
  width: 100%;
  border-collapse: collapse;
  flex: 1;
`;

const TableContainer = styled.div`
  height: calc(100vh - 148px);
  width: 100%;
  border: 1px solid ${colors.steelBlue};
  display: flex;
  flex-direction: column;
`;

const StyledHeader = styled.th`
  position: sticky;
  position: -webkit-sticky;
  padding: 10px;
  justify-content: center;
  background-color: ${colors.primary};
  top: -1px;
  z-index: 10;
  border: 1px solid ${colors.white};
`;

export { StyledTable, TableContainer, StyledHeader };
