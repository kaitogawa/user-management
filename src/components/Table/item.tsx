import React from 'react';
import PropTypes from 'prop-types';
import { User } from '../../models/user';
import { CustomSwitch } from '../Switch';
import { CustomButton } from '../Button/styled';

interface Props {
  item: User;
  style?: React.CSSProperties;
  sendMessage?: () => void;
  toggleActivateUser?: () => void;
}

export const TableItem: React.FC<Props> = props => {
  const { item, style, sendMessage, toggleActivateUser } = props;
  return (
    <tr style={style}>
      <td>{item.id}</td>
      <td>{item.fullName}</td>
      <td>{item.email}</td>
      <td>{item.status ? 'Active' : 'Inactive'}</td>
      <td
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          padding: 5,
        }}
      >
        <CustomSwitch isChecked={item.status} onChange={toggleActivateUser} />
        <CustomButton onClick={sendMessage}>Send Mail</CustomButton>
      </td>
    </tr>
  );
};

TableItem.propTypes = {
  item: PropTypes.any,
  style: PropTypes.any,
  sendMessage: PropTypes.func,
  toggleActivateUser: PropTypes.func,
};

TableItem.defaultProps = {
  style: undefined,
};
