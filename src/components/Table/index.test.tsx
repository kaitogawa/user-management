import React from 'react';
import { mount } from 'enzyme';
import dataDummy from '../../api/data/users.json';
import { CustomTable } from '.';
import { User } from '../../models/user.js';

describe('Table', () => {
  it('renders without crashing', () => {
    const users = [...dataDummy] as User[];

    const TableTest = mount(
      <CustomTable
        data={users}
        columns={['#', 'Name', 'Email', 'Status', 'Action']}
      />,
    );
    expect(TableTest).toMatchSnapshot();
  });
});
