import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { StyledTable, TableContainer } from './styled';
import { User } from '../../models/user';
import { TableHeader } from './header';
import { TableItem } from './item';
import { Header } from '../../models/header';
import { PaginationBar } from './paginationbar';
import colors from '../../theme/colors';

interface Props {
  data: User[];
  primaryColor?: string;
  secondaryColor?: string;
  sendMessage?: (index: number) => void;
  toggleActivateUser?: (index: number) => void;
  columns: Header[];
}

export const CustomTable: React.FC<Props> = props => {
  const {
    data,
    secondaryColor,
    primaryColor,
    sendMessage,
    toggleActivateUser,
    columns,
  } = props;

  const [sortedData, setSortedData] = useState(data);
  const [currentPage, setCurrentPage] = useState(1);
  const [sortType, setSortType] = useState('asc');
  const [sortColumn, setSortColumn] = useState(columns[0].key);
  const currentLimit = 20;
  // const [currentLimit, setCurrentLimit] = useState(15);

  useEffect(() => {
    setCurrentPage(1);
  }, [data.length]);

  useEffect(() => {
    const temp = _.orderBy(
      sortedData,
      [sortColumn],
      [sortType === 'asc' ? 'asc' : 'desc'],
    ) as User[];

    setSortedData(temp);
  }, [sortType, sortColumn]);

  return (
    <TableContainer>
      <div style={{ flex: 1, overflowY: 'scroll' }}>
        <StyledTable>
          <TableHeader
            columns={columns}
            sortType={sortType}
            sortColumn={sortColumn}
            setSortType={(type: string) => setSortType(type)}
            setSortColumn={(column: string) => setSortColumn(column)}
          />
          {data.length === 0 ? (
            <td colSpan={5} style={{ marginTop: 20 }}>
              data is empty
            </td>
          ) : (
            <tbody>
              {sortedData
                .slice(
                  currentLimit * (currentPage - 1),
                  currentLimit * currentPage,
                )
                .map((item, index) => (
                  <TableItem
                    item={item}
                    key={index}
                    style={{
                      backgroundColor:
                        index % 2 === 0
                          ? secondaryColor || colors.whiteSmoke
                          : primaryColor || colors.white,
                    }}
                    toggleActivateUser={() => {
                      if (toggleActivateUser) {
                        toggleActivateUser(item.id);
                      } else {
                        console.log(item);
                      }
                    }}
                    sendMessage={() => {
                      if (sendMessage) {
                        sendMessage(item.id);
                      } else {
                        console.log(item);
                      }
                    }}
                  />
                ))}
            </tbody>
          )}
        </StyledTable>
      </div>
      <PaginationBar
        total={data.length}
        limit={currentLimit}
        selected={currentPage}
        onClick={(page: number) => setCurrentPage(page)}
      />
    </TableContainer>
  );
};

CustomTable.propTypes = {
  data: PropTypes.array.isRequired,
  primaryColor: PropTypes.string,
  secondaryColor: PropTypes.string,
  sendMessage: PropTypes.func,
  toggleActivateUser: PropTypes.func,
  columns: PropTypes.array.isRequired,
};

CustomTable.defaultProps = {
  primaryColor: colors.white,
  secondaryColor: colors.whiteSmoke,
};
