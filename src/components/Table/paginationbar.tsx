import React from 'react';
import PropTypes from 'prop-types';
import { CustomButton } from '../Button/styled';
import colors from '../../theme/colors';

interface Props {
  limit: number;
  total: number;
  selected: number;
  onClick: (page: number) => void;
}

export const PaginationBar: React.FC<Props> = props => {
  const { limit, total, selected, onClick } = props;

  const page = [];

  const length = Math.ceil(total / limit);

  const firstSeenPage =
    selected > 3 ? (selected > length - 3 ? length - 5 : selected - 3) : 0;

  const lastSeenPage = selected < 3 ? 5 : selected + 2;

  for (let index = 0; index < length; index += 1) {
    page.push(index + 1);
  }

  return (
    <div
      style={{
        padding: 20,
        backgroundColor: `rgba(${colors.primaryRGB}, 0.2)`,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}
    >
      <div>
        {total && total > 0 ? (
          <span>
            Showing
            <span style={{ fontWeight: 'bold', padding: '0px 5px' }}>
              {(selected - 1) * limit + 1} -{' '}
              {limit * selected >= total ? total : limit * selected}
            </span>
            out of
            <span style={{ fontWeight: 'bold', padding: '0px 5px' }}>
              {total}
            </span>
            entries
          </span>
        ) : (
          <span>no entries</span>
        )}
      </div>
      <div>
        <div>
          {selected > 1 ? (
            <CustomButton onClick={() => onClick(selected - 1)}>
              Prev
            </CustomButton>
          ) : (
            ''
          )}
          {firstSeenPage > 0 ? (
            <CustomButton onClick={() => onClick(1)} disabled={selected === 1}>
              1
            </CustomButton>
          ) : null}
          {firstSeenPage > 1 ? '  ...  ' : ''}
          {page.slice(firstSeenPage, lastSeenPage).map(item => (
            <CustomButton
              key={item}
              onClick={() => onClick(item)}
              disabled={selected === item}
            >
              {item}
            </CustomButton>
          ))}
          {lastSeenPage < length - 1 ? '  ...  ' : ''}
          {lastSeenPage < length ? (
            <CustomButton
              onClick={() => onClick(length)}
              disabled={selected === length}
            >
              {length}
            </CustomButton>
          ) : null}
          {selected < length ? (
            <CustomButton onClick={() => onClick(selected + 1)}>
              Next
            </CustomButton>
          ) : (
            ''
          )}
        </div>
      </div>
    </div>
  );
};

PaginationBar.propTypes = {
  limit: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
  selected: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
};

PaginationBar.defaultProps = {
  limit: 0,
  total: 0,
  selected: 0,
  onClick: () => {},
} as Partial<Props>;
