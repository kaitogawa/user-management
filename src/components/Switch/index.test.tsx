import React from 'react';
import { mount } from 'enzyme';
import { CustomSwitch } from '.';

describe('CustomSwitch', () => {
  it('renders without crashing', () => {
    let checked = false;

    const CustomSwitchTest = mount(
      <CustomSwitch onChange={() => (checked = true)} isChecked={checked} />,
    );
    expect(CustomSwitchTest).toMatchSnapshot();
  });
});
