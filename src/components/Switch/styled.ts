import styled from 'styled-components';
import colors from '../../theme/colors';

const Wrapper = styled.label`
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
`;

const Checkbox = styled.input.attrs({
  type: 'checkbox',
})`
  display: none;
`;

const Slider = styled.span`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;

  transition: 0.4s;

  cursor: pointer;
  background-color: #ccc;
  border-radius: 17px;

  ${Checkbox}:checked + & {
    background-color: ${colors.forestGreen};
  }

  ${Checkbox}:focus + & {
    box-shadow: 0 0 1px ${colors.forestGreen};
  }

  &:before {
    position: absolute;
    content: '';
    left: 4px;
    bottom: 4px;

    transition: 0.4s;

    height: 26px;
    width: 26px;
    background-color: white;
    border-radius: 50%;

    ${Checkbox}:checked + & {
      transform: translateX(26px);
    }
  }
`;

export { Wrapper, Slider, Checkbox };
