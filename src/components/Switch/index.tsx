import React from 'react';
import PropTypes from 'prop-types';
import { Wrapper, Slider, Checkbox } from './styled';

interface Props {
  isChecked: boolean;
  onChange?: () => void;
}

export const CustomSwitch: React.FC<Props> = props => {
  const { isChecked, onChange } = props;
  return (
    <Wrapper>
      <Checkbox checked={isChecked} onChange={onChange} />
      <Slider />
    </Wrapper>
  );
};

CustomSwitch.propTypes = {
  isChecked: PropTypes.bool.isRequired,
  onChange: PropTypes.func,
};
