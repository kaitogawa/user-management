/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { StyledSnackbar } from './styled';

interface Props {
  style?: React.CSSProperties;
  duration?: number; // duration in ms
  children?: any;
  onFinish: () => void;
  show?: boolean;
}

export const CustomSnackbar: React.FC<Props> = props => {
  const { style, duration, children, onFinish, show } = props;

  useEffect(() => {
    if (show) {
      setTimeout(() => onFinish(), duration || 5000);
    }
  }, [show]);

  return (
    <StyledSnackbar show={show} style={style}>
      {children}
    </StyledSnackbar>
  );
};

CustomSnackbar.propTypes = {
  style: PropTypes.any,
  duration: PropTypes.number,
  children: PropTypes.any,
  onFinish: PropTypes.func.isRequired,
  show: PropTypes.bool,
};
