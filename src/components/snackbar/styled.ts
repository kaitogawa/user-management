import styled, { keyframes } from 'styled-components';
import colors from '../../theme/colors';

const fadeIn = keyframes`
  from {top: 0; opacity: 0;}
  to {top: 24px; opacity: 1;}
`;

const fadeOut = keyframes`
  from {top: 24px; opacity: 1;}
  to {top: 0; opacity: 0;}
`;

const StyledSnackbar = styled('div')<{ show?: boolean }>`
  visibility: ${props =>
    props.show
      ? 'visible'
      : 'hidden'}; /* Hidden by default. Visible on click */
  min-width: 250px; /* Set a default minimum width */
  display: flex;
  flex-direction: row;
  align-items: center;
  background-color: ${colors.forestGreen}; /* Black background color */
  color: #fff; /* White text color */
  border-radius: 2px; /* Rounded borders */
  padding: 16px; /* Padding */
  position: fixed; /* Sit on top of the screen */
  z-index: 1; /* Add a z-index if needed */
  left: 24px; /* Center the snackbar */
  top: 24px; /* 30px from the bottom */
  animation: ${props => (props.show ? fadeIn : fadeOut)} 0.25s linear;
  transition: visibility 0.25s linear;
`;

export { StyledSnackbar };
