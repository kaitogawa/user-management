import React from 'react';
import { mount } from 'enzyme';
import { CustomSnackbar } from '.';

describe('Snackbar', () => {
  it('renders without crashing', () => {
    let visible = true;

    const SnackbarTest = mount(
      <CustomSnackbar show={visible} onFinish={() => (visible = false)}>
        <div>test</div>
      </CustomSnackbar>,
    );
    expect(SnackbarTest).toMatchSnapshot();
  });
});
