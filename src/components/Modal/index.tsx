/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { ModalWrapper, ModalBoxSetup, ModalBg } from './styled';

interface Props {
  visible?: boolean;
  dismiss?: () => void;
  children?: any;
}

export const CustomModal: React.FC<Props> = props => {
  const { visible, dismiss, children } = props;

  const [modalShow, setModalShow] = useState(false);

  useEffect(() => {
    if (!visible) {
      setTimeout(() => setModalShow(false), 250);
    } else {
      setModalShow(true);
    }
  }, [visible]);

  return (
    <React.Fragment>
      {modalShow ? (
        <ModalWrapper show={visible}>
          <ModalBoxSetup>{children}</ModalBoxSetup>
          <ModalBg onClick={dismiss} />
        </ModalWrapper>
      ) : null}
    </React.Fragment>
  );
};

CustomModal.propTypes = {
  visible: PropTypes.bool,
  dismiss: PropTypes.func,
  children: PropTypes.node,
};
