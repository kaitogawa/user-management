import React from 'react';
import { mount } from 'enzyme';
import { CustomModal } from '.';

describe('Modal', () => {
  it('renders without crashing', () => {
    const ModalTest = mount(
      <CustomModal visible>
        <div>test</div>
      </CustomModal>,
    );
    expect(ModalTest).toMatchSnapshot();
  });
});
