import styled, { keyframes } from 'styled-components';

const fadeIn = keyframes`
  from { opacity: 0;}
  to { opacity: 1;}
`;

const fadeOut = keyframes`
  from { opacity: 1;}
  to { opacity: 0;}
`;

export const ModalWrapper = styled('div')<{ show?: boolean }>`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  z-index: 1050;
  display: flex;
  align-items: center;
  visibility: ${props => (props.show ? 'visible' : 'hidden')};
  animation: ${props => (props.show ? fadeIn : fadeOut)} 0.25s linear;
  transition: visibility 0.25s linear;
`;
export const ModalBoxSetup = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  width: 40%;
  overflow: hidden;
  padding: 16px;
  margin: auto;
  box-sizing: border-box;
  z-index: 1;
  box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.04);
  background: #fff;
  border: 0.5px solid #e8e8e8;
`;
export const ModalBg = styled.div`
  width: 100vw;
  height: 100vh;
  z-index: 0;
  background: rgba(0, 0, 0, 0.5);
`;
