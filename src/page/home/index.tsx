/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from 'react';
import { User } from '../../models/user';
import { HomeContainer, TextInput } from './styled';
import { CustomTable } from '../../components/Table';
import dataDummy from '../../api/data/users.json';
import { MessageModal } from './MessageModal';

export interface IHomeProps {
  history?: null;
}

export interface IHomeState {
  users: User[];
  modalVisible: boolean;
  selectedIndexMessage: number | null;
  searchKeyword: string;
}

const tableColumns = [
  { description: '#', key: 'id' },
  { description: 'Name', key: 'fullName' },
  { description: 'Email', key: 'email' },
  { description: 'Status', key: 'status' },
  { description: 'Action', key: null },
];

export default class Home extends React.Component<IHomeProps, IHomeState> {
  constructor(props: IHomeProps) {
    super(props);

    this.state = {
      users: dataDummy,
      modalVisible: false,
      selectedIndexMessage: null,
      searchKeyword: '',
    };
  }

  modalMessageVisible = (id: number) => {
    const { users } = this.state;

    const usersTemp = [...users];

    const index = usersTemp.findIndex(item => item.id === id);

    if (index >= 0) {
      this.setState({ modalVisible: true, selectedIndexMessage: index });
    }
  };

  dismissMessageVisible = () => {
    this.setState({ modalVisible: false, selectedIndexMessage: null });
  };

  toggleActivateUser = (id: number) => {
    const { users } = this.state;

    const usersTemp = [...users];

    const index = usersTemp.findIndex(item => item.id === id);

    if (index >= 0) {
      usersTemp[index].status = !usersTemp[index].status;
      this.setState({ users: usersTemp });
    }
  };

  render() {
    const {
      users,
      modalVisible,
      selectedIndexMessage,
      searchKeyword,
    } = this.state;
    return (
      <HomeContainer>
        <div style={{ height: 100, width: '100%' }}>
          <div style={{ fontSize: 24, textAlign: 'center' }}>
            User Management
          </div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'row-reverse',
              marginTop: 10,
            }}
          >
            <TextInput
              style={{ width: '20%' }}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                // this.tempKeyword = e.target.value;
                this.setState({ searchKeyword: e.target.value });
              }}
              placeholder="Search by keyword..."
            />
          </div>
        </div>
        <CustomTable
          data={users.filter((o: User) => {
            const test = { ...o } as {
              [key: string]: any;
            };
            return Object.keys(test).some((k: any) =>
              test[k]
                .toString()
                .toLowerCase()
                .includes(searchKeyword.toLowerCase()),
            );
          })}
          sendMessage={this.modalMessageVisible}
          toggleActivateUser={this.toggleActivateUser}
          columns={tableColumns}
        />
        <MessageModal
          modalVisible={modalVisible}
          dismissMessageVisible={this.dismissMessageVisible}
          users={users}
          selectedIndexMessage={selectedIndexMessage}
        />
      </HomeContainer>
    );
  }
}
