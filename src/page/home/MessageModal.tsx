import React, { useState } from 'react';
import { MdCancel } from 'react-icons/md';
import PropTypes from 'prop-types';
import { TextInput, TextBox } from './styled';
import { CustomButton } from '../../components/Button/styled';
import { User } from '../../models/user';
import colors from '../../theme/colors';
import { CustomModal } from '../../components/Modal';
import { FaCheck } from 'react-icons/fa';
import { CustomSnackbar } from '../../components/snackbar';

interface Props {
  selectedIndexMessage?: number | null;
  users: User[];
  dismissMessageVisible: () => void;
  modalVisible?: boolean;
}

export const MessageModal: React.FC<Props> = props => {
  const {
    selectedIndexMessage,
    users,
    dismissMessageVisible,
    modalVisible,
  } = props;

  const [alertVisible, setAlertVisible] = useState(false);

  return (
    <div>
      <CustomModal visible={modalVisible} dismiss={dismissMessageVisible}>
        <div
          style={{
            flexDirection: 'row',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <div style={{ textAlign: 'left', flex: 1 }}>
            Send mail to{' '}
            {typeof selectedIndexMessage === 'number'
              ? users[selectedIndexMessage].fullName
              : ''}
          </div>
          <MdCancel
            color={colors.danger}
            size={32}
            style={{ cursor: 'pointer' }}
            onClick={dismissMessageVisible}
          />
        </div>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            paddingTop: 16,
          }}
        >
          <TextInput placeholder="Title" />
          <TextBox placeholder="Message" rows={10} />
          <div
            style={{
              display: 'flex',
              flexDirection: 'row-reverse',
              paddingTop: 16,
            }}
          >
            <CustomButton
              style={{ marginRight: 0 }}
              onClick={() => {
                dismissMessageVisible();
                setAlertVisible(true);
              }}
            >
              SEND
            </CustomButton>
          </div>
        </div>
      </CustomModal>
      <CustomSnackbar
        onFinish={() => setAlertVisible(false)}
        show={alertVisible}
        duration={3000}
      >
        <FaCheck color={colors.white} size={24} style={{ marginRight: 10 }} />
        Your message has been sent!
      </CustomSnackbar>
    </div>
  );
};

MessageModal.propTypes = {
  users: PropTypes.array.isRequired,
  selectedIndexMessage: PropTypes.number,
  dismissMessageVisible: PropTypes.func.isRequired,
  modalVisible: PropTypes.bool,
};
