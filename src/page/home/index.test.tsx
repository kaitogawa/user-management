import React from 'react';
import { mount } from 'enzyme';
import Home from '.';

describe('HomePage', () => {
  it('renders without crashing', () => {
    const HomeTest = mount(<Home />);
    expect(HomeTest).toMatchSnapshot();
  });
});
