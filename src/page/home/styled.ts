import styled from 'styled-components';

const HomeContainer = styled.div`
  display: flex;
  padding: 24px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-content: center;
`;

const TextBox = styled.textarea`
  font-size: 16px;
  padding: 10px;
  margin-top: 16px;
  resize: none;
`;

const TextInput = styled.input`
  font-size: 16px;
  padding: 10px;
`;

export { HomeContainer, TextBox, TextInput };
