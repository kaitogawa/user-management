## Component Overviews

<img src="./images/screenshot-1.png" width="480px" >
<img src="./images/screenshot-2.png" width="480px" >

User Management is an Managing users apps that easy to use either with on large scale apps that has feature :

- Activate/Deactivate a user
- Send a message/mail to the user
- Search data with keyword
- pagination bar
- data sort
- etc.

some components & effects is created only using primitives components & some css inside with library `styled-components` for easier styling components with CSS styling.

## Available Scripts

In the project directory, you can run:

## `yarn install || npm install``

You must run this first to install all dependencies.

### `yarn start || npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test || npm test`

Launches the test runner in the interactive watch mode.<br />

### `yarn build || npm run-script build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!
